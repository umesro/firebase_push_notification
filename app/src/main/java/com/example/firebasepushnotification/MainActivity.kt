package com.example.firebasepushnotification

import android.Manifest
import android.content.ContentValues
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging

class MainActivity : AppCompatActivity() {
    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                // Permission granted, you can now send notifications
            } else {
                // Permission denied, you can't send notifications
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FirebaseApp.initializeApp(this)

        getToken()

        /*For Notification Permission*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU &&
            ContextCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED
        ) {
            // We don't have the permission, request it
            requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
        } else {
            // We already have the permission, do whatever you need to do
        }


        /* below code work when our application is Closed  When App is in background then Notification data we get in main or landing activity*/
        if (intent.extras != null) {
            val url = intent.extras!!["url"]
            if (url.toString().startsWith("http")) {
                println("url or keydata: "+url.toString() )
            }
        }
    }

    private fun getToken() {
        try {
            FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(
                        ContentValues.TAG,
                        "Fetching FCM registration token failed",
                        task.exception
                    )
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                var devicetoken = task.result
                if (devicetoken != null && devicetoken != "") {
                    println("++++   "+ devicetoken)

                }
            })

        } catch (e: Exception) {
            println("++++e   "+ e.message)
        }
    }
}

/* Done Now test it*/