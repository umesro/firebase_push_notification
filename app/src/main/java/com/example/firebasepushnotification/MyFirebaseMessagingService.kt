package com.example.firebasepushnotification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.util.Log
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {
    private val channel_id = "web_app"

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

//        println("++++++ " + Gson().toJson(remoteMessage))

        if (remoteMessage.data.isNotEmpty()) {
            val data = remoteMessage.data
            val title = data["title"]
            val body = data["body"]
            val url = data["url"]
//            above is optional its key

            if (title != null && body != null && url != null) {
                showNotification(title, body, url)
            }
        }
    }

    private fun showNotification(title: String, message: String, url: String) {
        val intent: Intent = if (url != null && url.startsWith("http")) {
            Intent(Intent.ACTION_VIEW, Uri.parse(url))
        } else {
//            this code for app flow... where you want to land your notification after click

//            if (AppPreference.getPreferenceValueByKey(this, AppPreference.INSTITUTE_ID).toString() != "") {
//                Intent(this, InstHomeActivity::class.java)
//            } else {
            Intent(this, MainActivity::class.java)
//            }
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this, 0, intent,
            PendingIntent.FLAG_MUTABLE
        )


        val contentView = RemoteViews(packageName, R.layout.notification)
        contentView.setTextViewText(R.id.title, title)
        contentView.setTextViewText(R.id.message, message)


        val notificationBuilder = NotificationCompat.Builder(this, channel_id)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setAutoCancel(true)
            .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
            .setOnlyAlertOnce(true)
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setLights(Color.BLUE, 500, 500)// Set priority to PRIORITY_MAX

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            notificationBuilder.setStyle(NotificationCompat.DecoratedCustomViewStyle())
        }

        notificationBuilder.setCustomContentView(contentView)

        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                channel_id,
                "web_app",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(notificationChannel)
        }

        notificationManager.notify(0, notificationBuilder.build())
    }


    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")
        sendRegistrationToServer(token)
    }

    private fun sendRegistrationToServer(token: String?) {
        Log.d(TAG, "sendRegistrationTokenToServer($token)")
        // Send the registration token to your server if needed
    }

    companion object {
        private const val TAG = "MyFirebaseMsgService"
    }
}
